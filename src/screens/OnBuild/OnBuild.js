import React from "react";
import { Col } from "reactstrap";

const OnBuild = (props) => {
    return (
        <>
            <div
                style={{
                    backgroundImage: `url(${require("assets/img/food.jpg")})`,
                    minHeight: "100vh",
                    minWidth: "100%",
                    backgroundSize: "cover",
                    backgroundPosition: "center",
                    display: "flex",
                }}
            >
                <Col
                    style={{
                        display: "flex",
                        flexDirection: "column",
                        justifyContent: "center",
                        alignItems: "center",
                        flexWrap: "wrap",
                        backgroundColor: "rgba(0, 0, 0, 0.30)",
                    }}
                >
                    <img
                        alt="bg"
                        src={require("assets/img/LOGO_CORSO.png")}
                        style={{ height: 200 }}
                    />
                    <text
                        style={{
                            color: "white",
                            fontSize: 30,
                            textAlign: "center",
                        }}
                    >
                        Estamos trabajando en un sitio nuevo para ti
                        próximamente
					</text>
                </Col>
            </div>
        </>
    );
};

export default OnBuild;
