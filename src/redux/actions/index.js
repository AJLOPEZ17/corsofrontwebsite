/**
 * @fileoverview Index Redux actions
 * exportación de las funciones de Redux
 * @version 1.0
 * @author César Paul Hernández Camacho
 * @date 30/10/2020
 * @copyright 2020 Industrias RESSER S.A de C.V
 * 
 */

export * from './auth';