/**
 * @fileoverview Auth 
 * Funciones a realizar en redux en base a la autentificación del usuario
 * @version 1.0
 * @author César Paul Hernández Camacho
 * @date 30/10/2020
 * @copyright 2020 Industrias RESSER S.A de C.V
 * 
 */

export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS';

export const loginUser = () => (dispatch) => {
  dispatch({
    type: LOGIN_SUCCESS
  })
};

export const logoutUser = () => (dispatch) => {
  dispatch({
    type: LOGOUT_SUCCESS
  })
};
