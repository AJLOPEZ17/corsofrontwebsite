export { default as AdminLayout } from "screens/Admin/Admin";

export { default as HomeLayout } from "screens/HomeLayout/HomeLayout";

export { default as OnBuild } from "screens/OnBuild/OnBuild";
